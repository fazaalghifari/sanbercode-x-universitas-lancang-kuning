<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $casts = Cast::all();
        return view('cast.cast', ['casts' => $casts]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi',
        ]);

        $cast = new Cast();
        $cast->name = $request->input('nama');
        $cast->umur = $request->input('umur');
        $cast->bio = $request->input('bio');
        $cast->save();

        return redirect('/cast')->with('success', 'Cast created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast.bio', ['cast' => $cast]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi',
        ]);

        cast::where('id',$id)
            ->update([
                'name' =>$request->input('nama'),
                'umur' =>$request->input('umur'),
                'bio' =>$request->input('bio'),
            ]);
            return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        cast::where('id', $id)->delete();

        return redirect ('/cast');
    }
}
