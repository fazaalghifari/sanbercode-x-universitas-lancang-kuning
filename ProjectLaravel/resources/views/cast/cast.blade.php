@extends('layouts.master')

@section('judul')
Halaman Cast
@endsection

@section('content')
<a href="{{ route('cast.create') }}" class="btn btn-sm btn-primary">Tambah</a>

<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key => $item)
        <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td>{{ $item->name }}</td>
            <td>{{ $item->umur }}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Bio</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <form action="{{ route('cast.destroy', $item->id) }}" method="POST" style="display:inline;">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Lanjutkan Hapus?')">Delete</button>
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="4" class="text-center">Cast belum ada</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection
