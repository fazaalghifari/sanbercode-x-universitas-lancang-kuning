@extends('layouts.master')

@section('judul')
Halaman Bio
@endsection

@section('content')
<div class="container">
    <h2>{{ $cast->name }}</h2>
    <p>{{ $cast->bio }}</p>
</div>
<a href="/cast" class="btn btn-secondary">Back</a>
@endsection
