<?php 
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo " Name : " . $sheep->name . "<br>"; // "shaun"
echo " Legs : " . $sheep->legs . "<br>"; // 4
echo " Cold Blooded : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$kodok = new frog("buduk");

echo " Name : " . $kodok->name . "<br>"; // "shaun"
echo " Legs : " . $kodok->legs . "<br>"; // 4
echo " Cold Blooded : " . $kodok->cold_blooded . "<br> ";
echo $kodok->jump("hop hop");

$sunggokong = new ape("kera sakti");

echo " Name : " . $sunggokong->name . "<br>"; // "shaun"
echo " Legs : " . $sunggokong->legs . "<br>"; // 4
echo " Cold Blooded : " . $sunggokong->cold_blooded . "<br> ";
echo $sunggokong->yell("Auooo");

?>
