<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    echo "<h4> soal 1 </h4>";

    $string = "php is never old";
    echo "panjang string : " . strlen($string) . "<br>";
    echo "jumlah kata : " . str_word_count($string) . "<br>";

    echo "<h4> first sentence </h4>";

    $first_sentence = "Hello PHP!";
    echo "panjang string : " . strlen($first_sentence) . "<br>";
    echo "jumlah kata : " . str_word_count($first_sentence) . "<br>";

    echo "<h4> secound sentence </h4>";

    $secound_sentence = "i'm ready for the challenges";
    echo "panjang string : " . strlen($secound_sentence) . "<br>";
    echo "jumlah kata : " . str_word_count($secound_sentence) . "<br>";

    echo "<h4> soal 2 </h4>";

    $string2 = "I love PHP";
        
    echo "<label>String: </label> \"$string2\" <br>";
    echo "<br>Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
    // Lanjutkan di bawah ini
    echo "<br> Kata kedua: " . substr($string2, 2, 5) . "<br>";
    echo "<br> Kata Ketiga: " . substr($string2, 7, 10 ) . "<br>";

    echo "<h4> soal 3 </h4>";

    $string3 = "PHP is old but sexy!";
    echo "String: \"$string3\" "."<br>"; 
    echo "<br>ganti string : " . str_replace("sexy","awesome",$string3);

    ?>
</body>
</html>
