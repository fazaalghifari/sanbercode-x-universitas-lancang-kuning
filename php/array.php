<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Array</h1>
    <?php 
    echo "<h4> soal 1 </h4>";
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
    $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];

    echo "<h4> array kids </h4>";
    print_r($kids);
    echo "<h4> array adults </h4>";
    print_r($adults);

    echo "<h4> soal 2 </h4>";

    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: " . count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> . $kids[0] . </li>";
    echo "<li> . $kids[1] . </li>";
    echo "<li> . $kids[2] . </li>";
    echo "<li> . $kids[3] . </li>";
    echo "<li> . $kids[4] . </li>";
    echo "<li> . $kids[5] . </li>";
    echo "</ol>";

    echo "<h4> soal 3 </h4>";

    $arrmulti = [
        ["name" => "will byers" , "age" => "12" , "aliases" => "will the wise", "status" => "Alive"],
        ["name" => "Mike wheeler" , "age" => "12" , "aliases" => "dungeon master", "status" => "Alive"],
        ["name" => "jim hopper" , "age" => "43" , "aliases" => "chief hopper", "status" => "deceased"],
        ["name" => "eleven" , "age" => "12" , "aliases" => "el", "status" => "Alive"]
    ];
    echo "<pre>";
    print_r($arrmulti);
    echo "</pre>";


    ?>
</body>
</html>
