<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php 
    echo "<h4> soal 1 </h4>";
    echo "<h5> looping pertama </h5>";
    
    $x=2;
    do{
        echo "$x - I love PHP <br>";
        $x+=2;
    }while ($x <=20);

    echo "<h5> looping kedua </h5>";

    for ($i=20; $i>=2; $i-=2){
        echo "$i - I love PHP <br>";
    }

    echo "<h4> soal 2 </h4>";

    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array numbers: ";
    print_r($numbers);
    //lakukan looping sini

    foreach($numbers as $nilai){
        $rest[]=$nilai %5;
    }
    echo "<br>";
    echo "Array sisa baginya adalah:  "; 
    print_r($rest);
    echo "<br>";

    echo "<h4> soal 3 </h4>";
    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach ($items as $arrIn){
        $output=[
            "id" => $arrIn[0],
            "name" => $arrIn[1],
            "price" => $arrIn[2],
            "description" => $arrIn[3],
            "source" => $arrIn[4]
        ];
        print_r($output);
        echo "<br>";
    }

    echo "<h4> soal 4 </h4>";
    echo "Asterix: ";
    echo "<br>";
    
    for ($i = 1; $i <= 5; $i++) { 
        for ($j = 1; $j <= $i; $j++) {
            echo "* "; 
        }
        echo "<br>"; 
    }

    ?>
</body>
</html>
